<?php
/**
* Template Name: Home-Template.
*/
?>

<div class="content-block">
  <?php while (have_posts()): the_post();?>
    <?php get_template_part('templates/content', 'page-home');?>
  <?php endwhile;?>
</div>

<div class="sep-60"></div>

<div id="start_sections" class="intro-widget-container container-fluid">
      <?php get_template_part('templates/home', 'blog');?>
</div>
