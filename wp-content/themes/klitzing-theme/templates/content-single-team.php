<?php $subheadline = rwmb_meta('rw_subhead');
$add_image1 = get_post_meta($post->ID, 'add_image1', true);
?>

<div class="sep-30"></div>
<div class="row">
  <div class="col-md-6">
    <h3 class="team-title"><?php the_title() ?></h3>
    <?php echo $post->post_content;  ?>
  </div>
  <div class="col-md-6">
    <?php if (has_post_thumbnail()) {
      echo '<div class="image-container"> ';
        the_post_thumbnail('main-stage', array('class' => 'img-responsive'));
         ?>
         <?php if (isset($add_image1) && !empty($add_image1 )) {   ?>
           <?php echo wp_get_attachment_image( $add_image1, array('700', '600'), "", array( "class" => "overlay-image img-responsive" ) );  ?>
           <?php } ?>
         <?php
        echo '<h2 class="subhead-image">'.$subheadline.'</h2>';
        echo '</div>';
      }
      ?>

  </div>
</div>
