
<?php $subheadline = rwmb_meta('rw_subhead'); ?>
<?php $theme_optionen_options = get_option( 'theme_optionen_option_name' ); ?>
<?php $text_color = $theme_optionen_options['text_farbe_auf_home_bild_0']; ?>
<?php $vid_url = $theme_optionen_options['video_url_1'];?>

<?php if (has_post_thumbnail()) {
  echo '<div class="image-container"> ';
  if (isset($vid_url) && !empty($vid_url)) {
    ?>
    <div class="arrow-container">
      <a id="start_sections_link" href="#start_sections">
        <img class="video-arrow" src="<?php echo get_template_directory_uri(); ?>/assets/images/PfeilimKreis-neu.svg" alt="arrow">
      </a>
    </div>
    <video width="100%" autoplay loop tabindex="0">
      <source src="<?php echo $vid_url;?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      Video tag not supported. Download the video <a href="<?php echo $vid_url;?>">here</a>.
    <video>
    <?php
    echo $vid_url;
  } else {

    the_post_thumbnail('main-stage', array('class' => 'img-responsive'));

    if ($text_color === 'weiss') {
      echo '<h2 class="subhead-image white">'.$subheadline.'</h2>';
    } elseif ($text_color === 'schwarz') {
      echo '<h2 class="subhead-image black">'.$subheadline.'</h2>';
    } else {
      echo '<h2 class="subhead-image">'.$subheadline.'</h2>';
    }
  }
  echo '</div>';
}
?>
<div class="sep-30"></div>
