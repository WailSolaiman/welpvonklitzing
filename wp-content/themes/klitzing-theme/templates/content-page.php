
<?php $subheadline = rwmb_meta('rw_subhead'); ?>

<?php if (has_post_thumbnail()) {
  echo '<div class="image-container"> ';
  the_post_thumbnail('main-stage', array('class' => 'img-responsive'));
  echo '<h2 class="subhead-image">'.$subheadline.'</h2>';
  echo '</div>';
}
?>
<div class="sep-30"></div>
<h1 class="page-title"><?php the_title() ?></h1>

<?php  the_content(); ?>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>'.__('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
