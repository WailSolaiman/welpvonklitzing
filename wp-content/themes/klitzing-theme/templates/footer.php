<div class="sep-70"></div>

<footer id="footer" class="container content-info">

  <div class="row">
    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Left 3/12')): ?>
    <?php endif;?>
    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Middle 6/12')): ?>
    <?php endif;?>
    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Right 3/12')): ?>
    <?php endif;?>
    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Down 3/12 ')): ?>
    <?php endif;?>
  </div>

  <div class="sep-30"></div>

  <div class="row">
    <div class="col-md-12 footer-down">
      <p>
      <a href="/impressum/">Impressum</a> © 2016 Welp von Klitzing
      </p>
    </div>
  </div>
</footer>
