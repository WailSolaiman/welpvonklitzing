<?php while (have_posts()) : the_post(); ?>
  <?php
  $project_image1 = get_post_meta($post->ID, 'project_images_image-1', true);
  $project_image2 = get_post_meta($post->ID, 'project_images_image-2', true);
  $project_image3 = get_post_meta($post->ID, 'project_images_image-3', true);
  $project_image4 = get_post_meta($post->ID, 'project_images_image-4', true);
  ?>
  <article <?php post_class(); ?>>
    <header>
      <?php the_post_thumbnail('main-stage', array('class' => 'img-responsive'));  ?>
      <div class="sep-70"></div>
      <h1 class="entry-title"><?php the_title(); ?></h1>

      <?php //get_template_part('templates/entry-meta'); ?>
    </header>

    <div class="entry-content">
      <?php the_content(); ?>
      <div class="sep-70"></div>
      <a href="#" class="back-button" onclick="window.history.go(-1); return false;">zurück</a>
      <div class="sep-30"></div>

      <div class="project-images">
        <?php if (isset($project_image1) && !empty($project_image1)) {
    ?>
          <div class="col-sm-3" >
            <div class="image-holder" style="background-image:url('<?php echo $project_image1 ?>')"></div>
          </div>
          <?php
} ?>
          <?php if (isset($project_image2) && !empty($project_image2)) {
    ?>
            <div class="col-sm-3" >
              <div class="image-holder" style="background-image:url('<?php echo $project_image2 ?>')"></div>
            </div>
            <?php
} ?>
            <?php if (isset($project_image3) && !empty($project_image3)) {
    ?>
              <div class="col-sm-3" >
                <div class="image-holder" style="background-image:url('<?php echo $project_image3 ?>')"></div>
              </div>
              <?php
} ?>
              <?php if (isset($project_image4) && !empty($project_image4)) {
    ?>
                <div class="col-sm-3" >
                  <div class="image-holder" style="background-image:url('<?php echo $project_image4 ?>')"></div>
                </div>
                <?php
} ?>
              </div>
            </div>
            <footer>
              <?php wp_link_pages(['before' => '<nav class="page-nav"><p>'.__('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
            </footer>
            <?php //comments_template('/templates/comments.php'); ?>
          </article>


        <?php endwhile; ?>
