<?php
remove_filter('the_content', 'wpautop');
$cat = get_cat_ID($post->post_title); //use page title to get a category ID
$args = array( 'posts_per_page' => -1, 'cat'=> $cat );
$posts = get_posts ($args);

$class = array('red-bg', 'blue-bg', 'green-bg'); //I make it an indexed array because associative array with same key is wrong and give you error
$key = 0;// start counter
$class_count = count($class); // take count of class array
if ($posts) {
  foreach ($posts as $post):
    setup_postdata($post); ?>
    <div class="row">
      <div class="col-md-12 <?php echo $class[$key];?> blog-image-holder">
        <div class="row">
          <div class="col-md-6">
            <?php the_post_thumbnail('main-stage', array('class' => 'img-responsive'));  ?>
          </div>

          <div class="col-md-6">
            <h6><?php the_title(); ?></h6>
            <p>
              <?php the_content(); ?>
            </p>
          </div>
        </div>


      </div>
    </div>
    <div class="sep-30">  </div>
    <?php if($key == $class_count-1){ // check when classes array values ends start count from 0 again
      $key = 0;
    }else{
      $key++;
    } ?>
  <?php endforeach;
}

?>
