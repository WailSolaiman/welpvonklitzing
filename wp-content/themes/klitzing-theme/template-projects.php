<?php
/**
* Template Name: Projekt-Template.
*/
?>

<?php $subheadline = rwmb_meta('rw_subhead'); ?>

<?php if (has_post_thumbnail()) {
  echo '<div class="image-container"> ';
  the_post_thumbnail('main-stage', array('class' => 'img-responsive'));
  echo '<h2 class="subhead-image">'.$subheadline.'</h2>';
  echo '</div>';
}
?>
<div class="sep-30"></div>

<?php //this builds the imagegrid gallery ?>

<div class="row">
  <div class="col-md-12">
    <div id="filters">
      <div class="filter-element"><a href="#" data-filter="*" class="selected btn btn-default">Chronologisch</a></div>
      <?php
      $terms = get_terms('category', array(
        'parent' => 0,
        'exclude' => 1,
        'orderby'    => 'term_order'
      ));
      $count = count($terms); //How many are they?
      if ($count > 0) {
        //If there are more than 0 terms
        foreach ($terms as $term) { //for each term:
          echo "<div class='filter-element'><a href='#' class='btn btn-default' data-filter='.".$term->slug."'>".$term->name."</a></div>\n";
          //create a list item with the current term slug for sorting, and name for label
        }
      }
      ?>
    </div>

  </div>
</div>

<div class="sep-30"></div>
<div id="isotope-list" class="row">
  <div class="col-md-12">
    <div class="row">
      <?php // WP_Query arguments
      $args = array(
        'post_type' => array('projects'),
        'posts_per_page' => -1,
      );
      // The Query
      $query = new WP_Query($args);
      // The Loop
      if ($query->have_posts()) {
        while ($query->have_posts()) {
          $query->the_post();
          //display image grid element
          $wpcats = wp_get_post_categories($post->ID);
          $cats = array();
          foreach ($wpcats as $c) {
            $cats[] = get_cat_slug($c);
          }
          $listcats = implode(' ', $cats);
          $posturl = get_post_permalink($post->ID);
          $title = get_the_title($post->ID);
          $classes = array(
            'col-sm-3',
            'box',
            $listcats,
          );
          $cat_classes = implode(' ', $classes);
          ?>
          <?php echo '<div class="'.$cat_classes.'">';
          //this makes the cat listing
          $cats = array();
          foreach ($wpcats as $c) {
            $cats[] = get_cat_name($c);
          }
          //this filters some content we dont want to display
          $filter = array('', '', '', '');
          foreach ($cats as $key => $value) {
            if (in_array($value, $filter)) {
              unset($cats[$key]);
            }
          }
          $lister = implode(', ', $cats);

          //get the custom project-image
          $project_image = get_post_meta( $post->ID, 'project_image1', true );

          echo '<a href="'.$posturl.'" class="image-post-link">';
          echo '<div class="mask"><h3>'.$title.'</h3><p class="category">'.$lister.'</p></div>';
          if (isset($project_image) && !empty($project_image)) {
            echo wp_get_attachment_image( $project_image , 'project-image' );;
          } else {
            echo '<img src="https://placeholdit.imgix.net/~text?txtsize=16&txt=noch+kein+Bild&w=262&h=275" class="img-responsive"';
          }

          echo '</a>';

          echo '</div>';
        }
      } else {
        // no posts found
      }

      // Restore original Post Data
      wp_reset_postdata(); ?>
    </div>
  </div>
</div>
