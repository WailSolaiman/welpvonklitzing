jQuery(document).ready(function($) {
  // This is the 'simple' lightbox

  function createParent() {
    $('body').append('<div class="lightbox-container"><div class="lightbox-container-inner"></div></div>');
  }
  createParent();

  // This counts, add a class, creates DOM elements
  function initClasses() {
    $('.image-holder').each(function(i) {
      $(this).attr('data-image', i);
      var bg_image = $(this).css('background-image');
      var imageClass = 'image' + $(this).attr('data-image');
      var imageNumber = $(this).attr('data-image');
      bg = bg_image.replace('url(', '').replace(')', '').replace(/\"/gi, "");

      $('.lightbox-container-inner').append('<div class="overlay image-holder ' + imageClass + '" data-image="' + imageNumber + '"><img src="' + bg + '" alt="" class="img-responsive"><div class="close-img">X</div></div>');
      //console.log('Just created ' + imageClass+ '.');
    });
  }
  initClasses();

  // Add Classes to First and Last item
  function addClassToFirstandLast() {
    $('.overlay.image-holder').first().addClass('first');
    $('.overlay.image-holder').last().addClass('last');
  }
  addClassToFirstandLast();

  // Hides First or Last Arrow If active
  function hideArrowIfFirstOrLastActive(){
    if ($('.image-holder.first').hasClass('active')) {
      $('.prev').hide();
    } else {
      $('.prev').fadeIn();
    }
    if ($('.image-holder.last').hasClass('active')) {
      $('.next').fadeOut();
    } else {
      $('.next').fadeIn();
    }
  }
  hideArrowIfFirstOrLastActive();

  // This opens the Lightbox
  $('.image-holder').on('click', function() {
    var imageClass = 'image' + $(this).attr('data-image');
    $('.' + imageClass).fadeIn().addClass('active');
    $('.lightbox-nav').fadeIn();
    posTheNavAndCloseButton();
    hideArrowIfFirstOrLastActive();
    //console.log('Just showed ' + imageClass + '.');
  });

  // This close the lightbox
  function closeMeNow() {
    $('.close-img').on('click', function(e) {
      //console.log('Lightbox closed.');
      $('.image-holder.active').fadeOut("slow", function() {
        $('.lightbox-nav').fadeOut("fast");
      }).removeClass('active');
      e.stopPropagation();
    });

  }
  closeMeNow();

  // This creates the Lightbox Navigation
  function createNav() {
    $('.lightbox-container').append('<a href="#" class="lightbox-nav prev"><</a><a href="#" class="lightbox-nav next">></a>');
  }
  createNav();

  // Resize Thumbs if Mobile (adjusted for all width ATM) REMINDER!!!
  function resizeThumbsIfMobile() {
    if ( $(window).width() < 1025 ) {
      $('.project-images .image-holder').each(function() {

        var width = $(this).width();
        $(this).css(
          {
            'height': width,
            'min-height': width 
          }
        );
      });
    }
  }
  resizeThumbsIfMobile();

  // Remove BREAKS from pragraphs beneath lead
  function removeBreaksUnderLead() {
    $('.lead + p br').remove();
  }
  removeBreaksUnderLead();

  // Create Mask-overlay
  function createMaskOverlay() {
    $('.project-images .image-holder').each(function() {

      $('<div class="mask plus-icon">').appendTo(this);
      var height = $(this).height();
      var width = $(this).width();
      $(this).find('.mask').css(
        {
          'height': height,
          'width' : width
        }
      );
    });

  }
  //createMaskOverlay();

  // Functions for navigating tru the Lightbox
  function moveLightLeft() {
    $('.image-holder.active').fadeOut().removeClass('active').prev().fadeIn().toggleClass('active');
    posTheNavAndCloseButton();
    hideArrowIfFirstOrLastActive();
  }

  function moveLightRight() {
    $('.image-holder.active').fadeOut().removeClass('active').next().fadeIn().toggleClass('active');
    posTheNavAndCloseButton();
    hideArrowIfFirstOrLastActive();
  }
  $(document).keydown(function(e){
    if (e.keyCode === 37) {
      var slideCount = $('.lightbox-container-inner .image-holder').length;
      var slideCounter = $('.lightbox-container-inner .image-holder.active').attr('data-image');

      if (slideCounter > 0) {
        moveLightLeft();
        $('.lightbox-nav').removeClass('hidden');
      } else {
        //console.log('This ist the Start.');
        $('.prev').addClass('nav-disabled');
      }
    }
  });
  $(document).keydown(function(e){
    if (e.keyCode === 39) {
      var slideCount = $('.lightbox-container-inner .image-holder').length;
      var slideCounter = $('.lightbox-container-inner .image-holder.active').attr('data-image');

      if (slideCounter <= slideCount - 2) {
        moveLightRight();
        $('.lightbox-nav').removeClass('hidden');
      } else {
        //console.log('This ist the End.');
        $('.next').addClass('hidden');
      }
    }
  });
  $('.prev').click(function(e) {
    e.preventDefault();
    var slideCount = $('.lightbox-container-inner .image-holder').length;
    var slideCounter = $('.lightbox-container-inner .image-holder.active').attr('data-image');

    if (slideCounter > 0) {
      moveLightLeft();
      $('.lightbox-nav').removeClass('hidden');
    } else {
      //console.log('This ist the Start.');
      $('.prev').addClass('hidden');
    }
  });

  $('.next').click(function(e) {
    e.preventDefault();
    var slideCount = $('.lightbox-container-inner .image-holder').length;
    var slideCounter = $('.lightbox-container-inner .image-holder.active').attr('data-image');

    if (slideCounter <= slideCount - 2) {
      moveLightRight();
      $('.lightbox-nav').removeClass('hidden');
    } else {
      //console.log('This ist the End.');
      $('.next').addClass('hidden');
    }
  });

  // This places the Nav at the correct Postion
  function posTheNavAndCloseButton() {
    var image = $('.image-holder.active img');
    var imageWidth = image.width();
    var imageHeight = image.height();
    var imagePosTop = imageHeight / 2;
    var imagePosTopTop = image.offset().top;
    var imagePixelToTop = $('.image-holder.active img').offset().top - $(document).scrollTop() - 15;
    var imagePosLeft = image.offset().left;
    var imagePosRight = $(window).width() - imageWidth - image.offset().left;
    $('.prev').animate({
      'padding-top' : imagePosTop +imagePixelToTop,
      left: 0,
      'padding-left' : imagePosLeft - 70
    }, 300, function() {
      // Animation complete.
    });
    $('.next').animate({
      'padding-top' : imagePosTop + imagePixelToTop,
      right: 0,
      'padding-right' : imagePosRight - 30
    }, 300, function() {
      // Animation complete.
    });
    $('.close-img').animate({
      'right' : imagePosRight - 30
    }, 300, function() {
      // Animation complete.
    });
  }

  console.log('Loaded Lightbox.');
});
