/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function () {
        // JavaScript to be fired on all pages
        //Overlay
        //add mask-overlay to lightbox galleries
        if ($('div').hasClass('gallery')) {
          $('.gallery-icon').find('a').prepend('<div class="mask plus-icon"></div>');
        }
        // This is the simple lightbox
        function closeMeNow() {
          $('.close-img').on('click', function () {
            $('.overlay').fadeOut();
          });
        }
        $('.image-holder').each(function () {
            var bg_image = $(this).css('background-image');
            new_gb = bg_image.replace('.jpg', '-262x275.jpg');
            $(this).css('background-image', new_gb);
          }

        );
        $('.image-holder').on('click', function () {
          var bg_image = $(this).css('background-image');
          bg = bg_image.replace('url(', '').replace(')', '').replace(/\"/gi, "");
          console.log(bg);

          $('body').append('<div class="overlay image-holder"><div class="close-img">X</div><img src="' + bg + '" alt="" class="img-responsive"></div>');
          closeMeNow();
        });

        //home scroll to Nav
        // save scrolled pixel to var
        $(window).scroll(function () {
          var $pix_scrolled = $(window).scrollTop();
          //hide scroll icon after x pixel scrolled
          if ($pix_scrolled > 300) {
            $(".arrow-down").fadeOut();
          } else {
            $(".arrow-down").fadeIn();
          }
        });
        //actual scroll function
        //check, if admin-bar is present, if yes, sub 32px from offset.top
        if (!$('body').hasClass('admin-bar')) {
          $(".arrow-down").click(function () {
            $('html, body').animate({
              scrollTop: $(".header-container").offset().top
            }, 2000);
          });
        } else {
          $(".arrow-down").click(function () {
            $('html, body').animate({
              scrollTop: $(".header-container").offset().top - 32
            }, 2000);
          });
        }
      },
      finalize: function () {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function () {
        // JavaScript to be fired on the home page

      },
      finalize: function () {
        // JavaScript to be fired on the home page, after the init JS
        $('.preloader').animate({
          'opacity': 1
        }, 2000, function () {
          // Animation complete.#
          $('.preloader').fadeOut(3000);
        });
      }
    },
    // About us page, note the change from about-us to about_us.
    'projekte': {
      init: function () {
        // JavaScript to be fired on the about us page
        //isotope
        var $container = $('#isotope-list'); //The ID for the list with all the blog posts
        $container.isotope({ //Isotope options, 'item' matches the class in the PHP
          itemSelector: '.box',
          layoutMode: 'fitRows'
        });

        // layout Isotope after each image loads
        $container.imagesLoaded().progress(function () {
          $container.isotope('layout');
        });

        //Add the class selected to the item that is clicked, and remove from the others
        var $optionSets = $('#filters'),
          $optionLinks = $optionSets.find('a');

        $optionLinks.click(function () {
          var $this = $(this);
          // don't proceed if already selected
          if ($this.hasClass('selected')) {
            return false;
          }
          var $optionSet = $this.parents('#filters');
          $optionSets.find('.selected').removeClass('selected');
          $this.addClass('selected');

          //When an item is clicked, sort the items.
          var selector = $(this).attr('data-filter');
          $container.isotope({
            filter: selector
          });

          return false;
        });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.

(function ($) {
  $('#start_sections_link').click(function (e) {
    console.log(e);
    var linkHref = $(this).attr('href');
    $('html, body').animate({
      scrollTop: $(linkHref).offset().top
    }, 700);
    e.preventDefault();
  });
})(jQuery);