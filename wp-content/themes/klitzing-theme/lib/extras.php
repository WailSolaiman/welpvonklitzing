<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
* Add <body> classes.
*/
function body_class($classes)
{
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__.'\\body_class');

/**
* Clean up the_excerpt().
*/
function excerpt_more()
{
  return ' &hellip; <a href="'.get_permalink().'">'.__('Continued', 'sage').'</a>';
}
add_filter('excerpt_more', __NAMESPACE__.'\\excerpt_more');

//Adding widget areas
/**
* Add SVG capabilities.
*/
function wpcontent_svg_mime_type($mimes)
{
  $mimes['svg'] = 'image/svg+xml';
  $mimes['svgz'] = 'image/svg+xml';

  return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__.'\\wpcontent_svg_mime_type');

//Adding widget areas
if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => 'Parallax Image',
    'id' => 'parallx_image',
    'before_widget' => '<div id="%1$s" class="widget %2$s parallax-image col-md-12">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));
  register_sidebar(array(
    'name' => 'Home Top',
    'id' => 'home_top',
    'before_widget' => '<div id="%1$s" class="widget %2$s col-md-12 col-xs-12">',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
  ));
  register_sidebar(array(
    'name' => 'Intro Widget',
    'id' => 'intro_widget',
    'before_widget' => '<div id="%1$s" class="widget intro-widget %2$s col-md-12 col-xs-12">',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
  ));
  register_sidebar(array(
    'name' => 'Image Gallery',
    'id' => 'home_img_gallery',
    'before_widget' => '<div id="%1$s" class="widget %2$s col-md-12 col-xs-12">',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
  ));
  register_sidebar(array(
    'name' => 'Home Down',
    'id' => 'home_down',
    'before_widget' => '<div id="%1$s" class="widget %2$s col-md-12 col-xs-12">',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
  ));
  register_sidebar(array(
    'name' => 'Footer Left 3/12',
    'id' => 'footer_left3',
    'before_widget' => '<div id="%1$s" class="widget %2$s col-md-4 col-xs-12 col-sm-6">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  ));
  register_sidebar(array(
    'name' => 'Footer Middle 6/12',
    'id' => 'footer_middle6',
    'before_widget' => '<div id="%1$s" class="widget %2$s col-md-4 col-xs-12 col-sm-6">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  ));
  register_sidebar(array(
    'name' => 'Footer Right 3/12',
    'id' => 'footer_right3',
    'before_widget' => '<div id="%1$s" class="widget %2$s col-md-4 col-xs-12 col-sm-6">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  ));
  register_sidebar(array(
    'name' => 'Footer Down 3/12',
    'id' => 'footer_down3',
    'before_widget' => '<div id="%1$s" class="widget %2$s col-md-3 col-xs-12 col-sm-6">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  ));
  register_sidebar(array(
    'name' => 'Footer Down Menu',
    'id' => 'footer_down_menu',
    'before_widget' => '<div id="%1$s" class="widget %2$s footer-down-menu">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>',
  ));
}

// Add meta Box

function kitzel_register_meta_boxes($meta_boxes)
{
  $prefix = 'rw_';

  // 1nd meta box - Add a subheading to Pages
  $meta_boxes[] = array(
    'title' => __('Sub-Headline', 'textdomain'),
    'post_types' => array('post', 'page'),
    'fields' => array(
      array(
        'name' => __('Title', 'textdomain'),
        'id' => $prefix.'subhead',
        'type' => 'text',
      ),
    ),
  );

  return $meta_boxes;
}
add_filter('rwmb_meta_boxes', __NAMESPACE__.'\\kitzel_register_meta_boxes');

//Add Some JS

function add_images_loaded()
{
  wp_register_script('images-loaded-init', get_template_directory_uri().'/assets/scripts/images-loaded.min.js', array('jquery'), true);
  wp_enqueue_script('images-loaded-init');
}
add_action('wp_enqueue_scripts', __NAMESPACE__.'\\add_images_loaded', 91);

function add_skrollr()
{
  wp_register_script('skrollr-init', get_template_directory_uri().'/assets/scripts/skrollr.min.js', array('jquery'), true);
  wp_enqueue_script('skrollr-init');
}
add_action('wp_enqueue_scripts', __NAMESPACE__.'\\add_skrollr', 99);

function add_isotope()
{
  wp_register_script('isotope-init', get_template_directory_uri().'/assets/scripts/jquery.isotope.min.js', array('jquery'), true);
  wp_enqueue_script('isotope-init');
}
add_action('wp_enqueue_scripts', __NAMESPACE__.'\\add_isotope', 92);

//Custom Image Sizes

add_image_size('main-stage', 1920, 570);
add_image_size('project-image', 262, 275, true);
add_image_size('team-image', 262, 275, true);

//crude browser detection

function browser_body_class($classes)
{
  global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

  if ($is_lynx) {
    $classes[] = 'lynx';
  } elseif ($is_gecko) {
    $classes[] = 'gecko';
  } elseif ($is_opera) {
    $classes[] = 'opera';
  } elseif ($is_NS4) {
    $classes[] = 'ns4';
  } elseif ($is_safari) {
    $classes[] = 'safari';
  } elseif ($is_chrome) {
    $classes[] = 'chrome';
  } elseif ($is_IE) {
    $classes[] = 'ie';
  } else {
    $classes[] = 'unknown';
  }

  if ($is_iphone) {
    $classes[] = 'iphone';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__.'\\browser_body_class');
