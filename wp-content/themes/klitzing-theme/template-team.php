<?php
/**
* Template Name: Team-Template.
*/
?>

<?php $subheadline = rwmb_meta('rw_subhead'); ?>

<?php if (has_post_thumbnail()) {
  echo '<div class="image-container"> ';
  the_post_thumbnail('main-stage', array('class' => 'img-responsive'));
  echo '<h2 class="subhead-image">'.$subheadline.'</h2>';
  echo '</div>';
}
?>
<div class="sep-30"></div>

<?php //this builds the imagegrid gallery ?>

<div id="isotope-list" class="row grid">
  <div class="col-md-12 image-grid">
    <div class="row">
      <?php // WP_Query arguments
      $args = array(
        'post_type' => array('team'),
        'posts_per_page' => -1,
      );
      // The Query
      $query = new WP_Query($args);
      // The Loop
      if ($query->have_posts()) {
        while ($query->have_posts()) {
          $query->the_post();
          //display image grid element
          $wpcats = wp_get_post_categories($post->ID);
          $cats = array();
          foreach ($wpcats as $c) {
            $cats[] = get_cat_slug($c);
          }
          $listcats = implode(' ', $cats);
          $posturl = get_post_permalink($post->ID);
          $title = get_the_title($post->ID);
          $classes = array(
            'col-md-3 col-xs-6',

            $listcats,
          );
          $cat_classes = implode(' ', $classes);
          $add_image1 = get_post_meta($post->ID, 'add_image1', true);
          ?>
          <?php echo '<div class="'.$cat_classes.'">';
          //this makes the cat listing
          $cats = array();
          foreach ($wpcats as $c) {
            $cats[] = get_cat_name($c);
          }
          //this filters some content we dont want to display
          $filter = array('', '', '', '');
          foreach ($cats as $key => $value) {
            if (in_array($value, $filter)) {
              unset($cats[$key]);
            }
          }
          $lister = implode(', ', $cats);
          if (get_the_content()) {
            echo '<a href="'.$posturl.'" class="image-post-link">';
          } else {
            echo '<a href="#" class="image-post-link empty-link">';
          }

          echo '<div class="team-image-holder">';
          if (has_post_thumbnail($post->ID)) {
            the_post_thumbnail('team-image' , array( 'class' => 'img-responsive' ));
          }
          if (isset($add_image1) && !empty($add_image1 )) {
            echo wp_get_attachment_image( $add_image1, array('262', '275', true), "", array( "class" => "overlay-image img-responsive" ) );
          }
          echo '</div>';
          echo '<h3>'.$title.'</h3>';
          echo '</a>';

          echo '</div>';
        }
      } else {
        // no posts found
      }

      // Restore original Post Data
      wp_reset_postdata(); ?>
    </div>
  </div>
</div>
