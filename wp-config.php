<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'welpvonklitzing');

/** MySQL database username */
define('DB_USER', 'welpvonklitzing');

/** MySQL database password */
define('DB_PASSWORD', 'welpvonklitzing');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p|vA#5(_m!bPIA|aO_mRG{[~=?OEjvEp),6&YW]%Y%X@_{ZQ{vBOZz4!S(3?5ol3');
define('SECURE_AUTH_KEY',  '^{x)3=l;D;{.H,2W`XugX5/l-s@gbq:_V->wY3yh2+VriT)=O1%[YpDH3N^9]bO0');
define('LOGGED_IN_KEY',    'zAPi,ly_AisS`MXd @krCnBD2dhjn858Q@fUnsnzi^/LTF>QYov5A*?2Y0e6=)h_');
define('NONCE_KEY',        'm(:bepC[(S)&@Gjm R1!Ot#+bdEZ=9J44443j]QAY(>R&P/)|V(7ljV@Og::_nHu');
define('AUTH_SALT',        'D-ti6(<[(oEcNYU}E6:JRUGpZ9~1bamml%7L;sVKxp>;x3;J XZwKH~|HQ;n-vdh');
define('SECURE_AUTH_SALT', 'ME, ?s}2Bz/?hFim<`OE!U8;5WSOuGd4<_B+KXyxO?e|z;r#$A7si#S@Z!PPj>]A');
define('LOGGED_IN_SALT',   '9N%_=&swWC/yW{G/a!s<P-kd{gIxdL>77;7K~6I=91Mk3v><K,H*YdNdn28YjQM6');
define('NONCE_SALT',       '<WHX5Svh:mjoOteDmW1gFHZRA+9@eS9m]mY+Q]#(i>Sc?#$lEt:~h@C`j#LsTjA1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_1337_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WPLANG', 'de_DE' );
